<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2><?= $table; ?></h2>
    <table class="table">
        <thead>
        <tr>
            <? foreach($columns as $column): ?>
                <th><?= $column; ?></th>
            <? endforeach; ?>
        </tr>
        </thead>
        <tbody>
        <? foreach($datas as $data): ?>
            <tr>
                <? foreach($columns as $col): ?>
                    <td contenteditable="true" myid="<?= $data['id']; ?>" class="editable table-<?= $table; ?> column-<?= $col; ?>">
                        <?= $data[$col]; ?>
                    </td>
                <? endforeach; ?>
            </tr>
        <? endforeach; ?>

        </tbody>
    </table>
</div>

<script>
    $(".editable").keydown(function(e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            //Enter key pressed, update the table
            classList = $(this)[0].classList;
            var newValue = $(this).text();
            var myId = $(this).attr('myid');

            console.log(newValue);

            var functionalList = [];

            for (var i = 0; i < classList.length; i++) {
                if (classList[i] !== 'editable') {
                    console.log(classList[i]);
                    var data = classList[i].split('-');
                    console.log(data);
                    functionalList[data[0]] = data[1];
                }
            }

            updateValue(functionalList['table'], functionalList['column'], newValue, myId);
        }
    });

    function updateValue(table, column, data, myId) {
        $.ajax({
            url: "/table/update",
            data: {id: myId, table:  table, column: column, data: data},
            method: "POST",
            success: function (result) {
                if(result == "success") {
                    alert('success');
                } else {
                    alert('error occured');
                }
            }
        })
    }
</script>

</body>
</html>
