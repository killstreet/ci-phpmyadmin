<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['table/update'] = 'databaseController/update';
$route['table/(:any)'] = 'databaseController/show/$1';


$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
