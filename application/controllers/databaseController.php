<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class databaseController extends CI_Controller {

    public function index($table)
    {
        echo "databaseController :  " . $table;
    }

    public function show($table)
    {
        $columns = $this->db->list_fields($table);
        $datas = $this->db->select('*')->from($table)->get()->result_array();

        $this->load->view('phpmyadmin/show', ['table' => $table, 'columns' => $columns, 'datas' => $datas]);
    }

    public function update()
    {
        $this->db->set($_POST['column'], $_POST['data']);
        $this->db->where('id', $_POST['id']);
        $this->db->update($_POST['table']);

        echo 'success';
    }
}
